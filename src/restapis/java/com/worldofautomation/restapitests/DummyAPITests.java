package com.worldofautomation.restapitests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.junit.Test;

public class DummyAPITests {
    @Test
    public void getAllEmployees() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        Response response = RestAssured.given().when().log().all().get("/employees")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
        //get data from db
        //validate
    }

    @Test
    public void getSingleEmployee() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        Response response = RestAssured.given().when().log().all().get("/employees/1")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }

    @Test
    public void deleteSingleEmployee() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        Response response = RestAssured.given().when().log().all().delete("delete/24")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }

    @Test
    public void createUser() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Henderson");
        jsonObject.put("salary", "101,000");
        jsonObject.put("age", "29");

        Response response = RestAssured.given().when().body(jsonObject.toString()).log().all().post("/create")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }

    @Test
    public void updateUser() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Henderson");
        jsonObject.put("salary", "109,000");
        jsonObject.put("age", "29");

        Response response = RestAssured.given().when().body(jsonObject.toString()).log().all().put("/update/97")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }

}
