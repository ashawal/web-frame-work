package com.worldofautomation;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase {
    private static WebDriver driver = null;

    public static WebDriver getDriver() {
        return driver;
    }

    private static ExtentReports extent;


    @Parameters({"url", "browserName", "cloud"})
    @BeforeMethod
    public static void setupConfig(String url, String browserName, boolean cloud) {
        if (cloud == true) {

            String bsUserName = "azharkautomation1";
            String bsAccessKey = "mFTz1sSxLoaBGPBJpFPQ";
            String urlForBS = "https://" + bsUserName + ":" + bsAccessKey + "@hub-cloud.browserstack.com/wd/hub";

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "80.0");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "Catalina");
            caps.setCapability("resolution", "1024x768");
            caps.setCapability("name", "Bstack-[Java] Sample Test");
            URL urll = null;
            try {
                urll = new URL(urlForBS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver = new RemoteWebDriver(urll, caps);

        } else {
            if (browserName.equalsIgnoreCase("Chrome")) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                driver = new ChromeDriver();
            } else if (browserName.equalsIgnoreCase("Mozilla")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                driver = new FirefoxDriver();
            }
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//other elements to load
        driver.get(url);  // <---this is where it's launching the browser
    }

    public static void waitFor(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public static void close() {
        //driver.close();
        driver.quit();
    }

    //****************EXTENT STARTED***********************|
    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }
    //*******************************EXTENT FINISHED***********************|


    public void validateIfTextIsAsExpected(String xpath, String expectedText) {
        WebElement element = driver.findElement(By.xpath("xpath"));
        String actualText = element.getText();
        Assert.assertEquals(actualText, expectedText);
    }

    public String getTextFromXpath(String xpath) {
        String text = driver.findElement(By.xpath(xpath)).getText();
        return text;
    }

    public static boolean validateIfXpathIsDisplayed(String xpath) {
    /*WebElement element = driver.findElement(By.xpath(xpath));
    boolean isDisplayed = element.isDisplayed();*/
        boolean ifDisplayed = driver.findElement(By.xpath(xpath)).isDisplayed();
        return ifDisplayed;
    }

    public static boolean validateListOfXpathIsDisplayed(String xpath) {
        List<WebElement> elementList = driver.findElements(By.xpath(xpath));
        boolean flag = false;
        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isDisplayed()) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static String getCurrentURL() {
        return driver.getCurrentUrl();
    }

    public static void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public static void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }
    public static void clickByLinkText(String linkText) {
        driver.findElement(By.id(linkText)).click();
    }

    public static void sendKeysById(String id, String keysToSend) {
        driver.findElement(By.id(id)).sendKeys(keysToSend);
    }

    public static void sendKeysByXpath(String xpath, String keysToSend) {
        driver.findElement(By.xpath(xpath)).sendKeys(keysToSend);

    }
    public static void switchToTab(int tabNum){
        ArrayList<String> tabList= new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabList.get(tabNum));
    }
}




