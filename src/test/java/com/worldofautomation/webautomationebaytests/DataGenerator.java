package com.worldofautomation.webautomationebaytests;

import com.worldofautomation.Utilities;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class DataGenerator {
    @DataProvider(name = "getSearchData")
    public Object[] getSearchData() {
        JSONArray jsonArray = Utilities.getJSONArray("src/test/resources/TestData.json");
        System.out.println(jsonArray); // square brackets
        JSONObject jsonObject = (JSONObject) jsonArray.get(1); // cast
        String data = (String) jsonObject.get("searchData");
        Object[] objects = new Object[1];
        objects[0] = data;
        return objects;
    }

    @DataProvider(name = "getSearchData2")
    public Object[][] getSearchData2() {
        return new Object[][]
                {{"Java", "Selenium"}};
    }
}
