package com.worldofautomation.webautomationebaytests.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage {
    @FindBy(id = "firstname")
    private WebElement firstName;
    @FindBy(id = "lastname")
    private WebElement lastName;

    public void fillFnameLName() {
        firstName.sendKeys("test@gmail.com");
        lastName.sendKeys("password");
    }
}
