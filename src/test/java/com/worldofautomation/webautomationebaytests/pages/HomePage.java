package com.worldofautomation.webautomationebaytests.pages;

import com.worldofautomation.ExtentTestManager;
import com.worldofautomation.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage extends TestBase {
    @FindBy(linkText = "register")
    private WebElement registerBtn;

    @FindBy(id ="gh-ac") //data, need a variable,
    private WebElement searchBar; // needs to be private page may refreshed

    @FindBy(id="gh-btn")
    private WebElement searchBtn;


    public void clickOnRegisterButton(){
        registerBtn.click();
    }

    public void validateUserIsOnHomePage() {
        String currUrl = TestBase.getDriver().getCurrentUrl();
        String expectedUrl= "https://www.ebay.com/";
        Assert.assertEquals(currUrl,expectedUrl);
        ExtentTestManager.log("url has been validated");
    }

    public void typeOnSearchBar(String data) {
        searchBar.sendKeys(data);
        ExtentTestManager.log(" able to type Java Books");
    }

    public void clickOnSearchButton() {
        searchBtn.click();
        ExtentTestManager.log(" search button has been clicked");

    }

    public void validateGhostTextInSearchBox() {
        String ghostText= searchBar.getAttribute("aria-label"); //from the xpath
        Assert.assertEquals(ghostText,"Search for anything");
        Assert.assertTrue(searchBar.isDisplayed());
        ExtentTestManager.log("Search for anything :Ghost text has been displayed");
    }
}
