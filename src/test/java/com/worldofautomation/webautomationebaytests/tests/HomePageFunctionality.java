package com.worldofautomation.webautomationebaytests.tests;

import com.worldofautomation.ExtentTestManager;
import com.worldofautomation.TestBase;
import com.worldofautomation.webautomationebaytests.DataGenerator;
import com.worldofautomation.webautomationebaytests.pages.HomePage;
import com.worldofautomation.webautomationebaytests.pages.RegisterPage;
import com.worldofautomation.webautomationebaytests.pages.SearchResultPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageFunctionality extends TestBase {
    private HomePage homePage;
    private RegisterPage registerPage;
    private SearchResultPage searchResultPage;

    @BeforeMethod
    private void setupInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        registerPage = PageFactory.initElements(getDriver(), RegisterPage.class);
        searchResultPage = PageFactory.initElements(getDriver(), SearchResultPage.class);
    }
    @Test
    public void ValidateUserAbleToRegister() {
        homePage.validateUserIsOnHomePage();
        homePage.clickOnRegisterButton();
        ExtentTestManager.log("clicked on register button");
        //registerPage.fillFnameLName();
        ExtentTestManager.log("filled in first and last name");
        //clickOnRegisterButt  //fillFNameLName
    }
    @Test(dataProviderClass = DataGenerator.class, dataProvider = "getSearchData")
    public void userBeingAbleToSearchAnItem(String data) {
        homePage.validateUserIsOnHomePage();

        homePage.typeOnSearchBar(data);
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(data);
    }
    @Test(enabled = false)
    public void validateUserIsAbleToSeeGhostText(){
        homePage.validateUserIsOnHomePage();
        homePage.validateGhostTextInSearchBox();
    }
}
