package com.worldofautomation.webautomationfacebooktests.tests;

import com.worldofautomation.TestBase;
import com.worldofautomation.webautomationfacebooktests.DataGenerator;
import com.worldofautomation.webautomationfacebooktests.pages.HomePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class HomePageFunctionality extends TestBase {
    HomePage homePage = null;
    @BeforeMethod
    public void setupInstances(){
        homePage = PageFactory.initElements(getDriver(),HomePage.class);

    }
    @Test(dataProviderClass = DataGenerator.class,dataProvider = "getCredentials")
    public void userShouldNotBeAbleToLoginWithInvalidCred(String username, String password){
        homePage.validateFaceBookIsDisplayed();
        homePage.sendUsernameAndPassInRqdFields(username,password);
        homePage.clickOnLoginBtn();
        }

    }


